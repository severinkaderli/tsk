use crate::task::Task;
use ansi_term::Colour::Green;
use clap::ArgMatches;

pub fn run(_matches: &ArgMatches, tasks: &mut Vec<Task>) {
    // Sort by due_date

    for (i, task) in tasks.iter().enumerate() {
        println!("{} {}", Green.paint(format!("{}:", i + 1)), task);
    }
}
