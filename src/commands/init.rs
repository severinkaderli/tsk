use clap::ArgMatches;
use serde_json::json;
use std::fs;
use std::fs::File;
use std::path::Path;

/**
 * Creates a tasks.json in the current directory.
 */
pub fn run(_matches: &ArgMatches) {
    let path = Path::new("tasks.json");
    if path.exists() {
        println!("tasks.json already exists");
    } else {
        let tasks_json = json!({"tasks": []});

        File::create(&path).expect("Can't create file!");
        fs::write(&path, tasks_json.to_string()).expect("Unable to write file");
        println!("Initialized tasks.json");
    }
}
