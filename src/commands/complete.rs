use crate::task::{Repetition, Task};
use chrono::Duration;
use clap::ArgMatches;

/**
 * Creates a tasks.json in the current directory.
 */
pub fn run(matches: &ArgMatches, tasks: &mut Vec<Task>) {
    let id = matches.value_of("ID").unwrap().parse::<usize>().unwrap() - 1;
    let task;
    if let Some(t) = tasks.get_mut(id) {
        task = t;
    } else {
        println!("Unknown task id: {}", id + 1);
        std::process::exit(1);
    }

    if let Some(repetition) = task.repetition() {
        match repetition {
            Repetition::DAILY => task.set_due_date(*task.due_date() + Duration::days(1)),
            Repetition::WEEKLY => task.set_due_date(*task.due_date() + Duration::days(7)),
            Repetition::MONTHLY => task.set_due_date(*task.due_date() + Duration::days(30)),
        };
    } else {
        tasks.remove(id);
    }
}
