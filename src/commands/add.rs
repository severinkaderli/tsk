use crate::task::{Repetition, Task};
use chrono::{Local, NaiveDate};
use clap::ArgMatches;
use regex::Regex;

/**
 * Creates a tasks.json in the current directory.
 */
pub fn run(matches: &ArgMatches, tasks: &mut Vec<Task>) {
    let description = matches
        .values_of("TASK")
        .unwrap()
        .collect::<Vec<&str>>()
        .join(" ");

    // Check for categories in task
    let mut category = "";
    let re = Regex::new(r"\s#(\w*)").unwrap();
    let captures = re.captures(&description);
    match captures {
        Some(captures) => category = captures.get(1).unwrap().as_str(),
        None => (),
    }
    let description = re.replace_all(&description, "").to_string();

    // Check for due date in task
    let due_date;
    let re = Regex::new(r"\s@(\d{4}-\d{2}-\d{2})").unwrap();
    let captures = re.captures(&description);
    match captures {
        Some(captures) => {
            let parse_result =
                NaiveDate::parse_from_str(captures.get(1).unwrap().as_str(), "%Y-%m-%d");
            match parse_result {
                Ok(date) => due_date = date,
                Err(err) => {
                    println!("Invalid date format: {}", err);
                    std::process::exit(1);
                }
            }
        }
        // If no date was supplied use todays date
        None => {
            due_date = Local::now().date().naive_local();
        }
    }
    let description = re.replace_all(&description, "").to_string();

    let mut repetition: Option<Repetition> = None;
    let re = Regex::new(r"\s\+(\w*)").unwrap();
    let captures = re.captures(&description);
    match captures {
        Some(captures) => {
            repetition = Some(Repetition::from(captures.get(1).unwrap().as_str()));
        }
        None => (),
    }
    let description = re.replace_all(&description, "").to_string();

    let task = Task::new(
        description.clone(),
        String::from(category),
        due_date,
        repetition,
    );
    // println!("Created task: {:?}", &task);
    tasks.push(task);
}
