use ansi_term::Colour::Blue;
use ansi_term::Colour::Red;
use ansi_term::Colour::Yellow;
use chrono::NaiveDate;
use serde::{Deserialize, Serialize};
use std::fmt;

#[derive(Serialize, Deserialize, Debug)]
pub enum Repetition {
    DAILY,
    WEEKLY,
    MONTHLY,
}

impl fmt::Display for Repetition {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Repetition::DAILY => write!(f, "Daily"),
            Repetition::WEEKLY => write!(f, "Weekly"),
            Repetition::MONTHLY => write!(f, "Monthly"),
        }
    }
}

impl Repetition {
    pub fn from(input: &str) -> Repetition {
        match input.to_uppercase().as_ref() {
            "DAILY" => Repetition::DAILY,
            "WEEKLY" => Repetition::WEEKLY,
            _ => {
                println!("Invalid repetition specifier");
                std::process::exit(1);
            }
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Task {
    description: String,
    category: String,
    due_date: NaiveDate,
    repetition: Option<Repetition>,
}

impl fmt::Display for Task {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let date_string = format!("[{}]", self.due_date());
        write!(f, "{}", Yellow.paint(date_string))?;

        if let Some(repetition) = &self.repetition {
            let repetition_string = format!("[{}]", repetition);
            write!(f, "{}", Blue.paint(repetition_string))?;
        }

        if self.category.len() > 0 {
            let category_string = format!("[{}]", self.category());
            write!(f, "{}", Red.paint(category_string))?;
        }

        write!(f, " {}", self.description())?;

        Ok(())
    }
}

impl Task {
    pub fn new(
        description: String,
        category: String,
        due_date: NaiveDate,
        repetition: Option<Repetition>,
    ) -> Task {
        Task {
            description: description,
            category: category,
            due_date: due_date,
            repetition: repetition,
        }
    }

    /// Get the due date of the task
    pub fn due_date(&self) -> &NaiveDate {
        &self.due_date
    }

    /// Set the due date of the task
    pub fn set_due_date(&mut self, due_date: NaiveDate) {
        self.due_date = due_date;
    }

    pub fn category(&self) -> &String {
        &self.category
    }

    /// Get the description of the task
    pub fn description(&self) -> String {
        self.description.clone()
    }

    pub fn repetition(&self) -> &Option<Repetition> {
        &self.repetition
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_display_impl_for_task() {
        let task = Task::new(
            String::from("Description"),
            String::from("Category"),
            NaiveDate::from_ymd(2018, 10, 12),
            None,
        );
        assert!(true);
        //assert_eq!("", format!("{}", task));
    }
}
