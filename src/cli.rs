use clap::{crate_authors, crate_description, crate_name, crate_version, App, Arg, SubCommand};
pub fn build_cli() -> App<'static, 'static> {
    App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .subcommand(
            SubCommand::with_name("init")
                .about("Creates a new tasks.json in the current directory."),
        )
        .subcommand(SubCommand::with_name("list").about("Lists all tasks"))
        .subcommand(
            SubCommand::with_name("add")
                .about("Adds a new task to tasks.json")
                .arg(
                    Arg::with_name("TASK")
                        .help("Sets the input file to use")
                        .required(true)
                        .multiple(true),
                ),
        )
        .subcommand(
            SubCommand::with_name("complete")
                .about("Complete task")
                .arg(
                    Arg::with_name("ID")
                        .help("The id of the task you want to complete")
                        .required(true),
                ),
        )
}
