extern crate regex;
use serde_json::json;
use std::fs;
use std::path::Path;

mod cli;
mod commands;
pub mod task;

fn main() {
    let path = Path::new("tasks.json");
    let matches = cli::build_cli().get_matches();
    let mut tasks: Vec<task::Task> = Vec::new();

    // TODO: Check if &path exists
    if !path.exists() && matches.subcommand().0 != "init" {
        println!("No tasks.json found in the current directory");
        std::process::exit(1);
    }

    if matches.subcommand().0 != "init" {
        let data = fs::read_to_string(&path).expect("Unable to read file");
        tasks = serde_json::from_str(&data).unwrap();
    }
    tasks.sort_by_key(|k| *k.due_date());
    match matches.subcommand() {
        ("init", Some(m)) => commands::init::run(m),
        ("add", Some(m)) => commands::add::run(m, &mut tasks),
        ("list", Some(m)) => commands::list::run(m, &mut tasks),
        ("complete", Some(m)) => commands::complete::run(m, &mut tasks),
        _ => println!("Unknown command"),
    }

    let json_data = json!(tasks);
    fs::write(&path, json_data.to_string()).expect("Unable to write file");
}
