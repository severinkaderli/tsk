extern crate clap;
use std::fs;
use clap::Shell;

include!("src/cli.rs");

fn main() {
    let completions_directory = "./target/completions";
    fs::create_dir_all(completions_directory).expect("Error");
    let mut app = build_cli();
    app.gen_completions(crate_name!(), Shell::Bash, completions_directory); 
    app.gen_completions(crate_name!(), Shell::Fish, completions_directory); 
    app.gen_completions(crate_name!(), Shell::Zsh, completions_directory); 
}
